package com.example.lynn.testdatabase;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lynn on 3/13/2015.
 */
public class MyView extends LinearLayout {

    public MyView(Context context) {
        super(context);

        MyDatabaseHelper helper = new MyDatabaseHelper(context,"faculty");

        SQLiteDatabase database = helper.getWritableDatabase();

        String[] columns = {"firstname","lastname"};

        Cursor cursor = database.query("faculty",columns,"",null,null,null,"lastname ASC");

        List<String> list = new ArrayList<>();

        if (cursor.moveToFirst())
           do {
               String firstName = cursor.getString(cursor.getColumnIndexOrThrow("firstname"));
               String lastName = cursor.getString(cursor.getColumnIndexOrThrow("lastname"));

               list.add(firstName + " " + lastName);
           } while (cursor.moveToNext());

        database.close();

        ArrayAdapter<String> adapter = new ArrayAdapter<>(context,android.R.layout.simple_list_item_1,list);

        Spinner faculty = new Spinner(context);

        faculty.setAdapter(adapter);

        addView(faculty);
    }

}
