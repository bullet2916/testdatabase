package com.example.lynn.testdatabase;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by lynn on 3/13/2015.
 */
public class MyDatabaseHelper extends SQLiteOpenHelper {
    private static int DATABASE_VERSION = 1;

    public MyDatabaseHelper(Context context,
                            String name) {
        super(context,name,null,DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase database) {
        database.execSQL("DROP TABLE IF EXISTS faculty");

        database.execSQL("CREATE TABLE faculty(firstname TEXT NOT NULL,lastname TEXT NOT NULL);");

        database.execSQL("INSERT INTO faculty values('Mark','Yampolskiy');");
        database.execSQL("INSERT INTO faculty values('Harold','Pardue');");
    }

    public void onUpgrade(SQLiteDatabase database,int oldVersion,int newVersion) {
        database.execSQL("DROP TABLE IF EXISTS faculty");

        onCreate(database);
    }



}
